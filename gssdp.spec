Name:          gssdp
Version:       1.0.2
Release:       7
Summary:       Resource discovery and announcement over SSDP
License:       LGPLv2+
URL:           http://www.gupnp.org/
Source0:       http://download.gnome.org/sources/gssdp/1.0/gssdp-%{version}.tar.xz

BuildRequires: chrpath gtk-doc pkgconfig vala >= 0.20 gobject-introspection-devel >= 1.36
BuildRequires: dbus-glib-devel GConf2-devel glib2-devel gtk3-devel libsoup-devel libxml2-devel

Requires:      dbus
Provides:      gssdp-utils
Obsoletes:     gssdp-utils

%description
A GObject-based API for handling resource discovery and announcement over SSDP.

%package       devel
Summary:       Development files for gssdp
Requires:      gssdp = %{version}-%{release}

%description   devel
This package contains development files for gssdp.

%package       help
Summary:       Documentation files for gssdp
Requires:      gssdp = %{version}-%{release}
BuildArch:     noarch
Provides:      gssdp-docs = %{version}-%{release}
Obsoletes:     gssdp-docs < %{version}-%{release}

%description   help
This package contains documentation files for gssdp.

%prep
%autosetup -p1

%build
%configure --disable-static
%make_build V=1

%install
%make_install
%delete_la

chrpath --delete %{buildroot}%{_bindir}/gssdp-device-sniffer

%check
make check %{?_smp_mflags} V=1

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc COPYING AUTHORS README NEWS
%dir %{_datadir}/gssdp
%{_libdir}/libgssdp-1.0.so.*
%{_libdir}/girepository-1.0/GSSDP-1.0.typelib
%{_bindir}/gssdp-device-sniffer
%{_datadir}/gssdp/gssdp-device-sniffer.ui

%files devel
%{_includedir}/gssdp-1.0
%{_libdir}/libgssdp-1.0.so
%{_libdir}/pkgconfig/gssdp-1.0.pc
%{_datadir}/gir-1.0/GSSDP-1.0.gir
%{_datadir}/vala/vapi/gssdp*

%files help
%{_datadir}/gtk-doc/html/gssdp

%changelog
* Wed Oct 30 2019 Lijin Yang <yanglijin@huawei.com> - 1.0.2-7
- Initial package
